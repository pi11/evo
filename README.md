## Javascript non-interactive evolution game - elephants vs leopards ##

Leopards search and eat elephants. If the leopard is full, it does not attack the elephant.
If the elephant's defense is higher than the leopard's attack, then the elephant will survive, and with some probability the leopard will die.
At the beginning of the game, the attack of the leopards is always higher than the defense of the elephants.

Both types of animals age and die. Adult animals can produce offspring that inherit the skills of their parents. In offspring, various traits can mutate both upward and downward.

   Pastures are a source of food for elephants. After the elephants eat all the food, the pasture disappears and after a while appears in another place of the playing field. 

Online - https://scurra.space/evo/ (Only Russian Interface)
