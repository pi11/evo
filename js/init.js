var DAY = 0;
var SPEED = 0;
function main_init(){
    console.log('Start');
    points_left = Number(points_el.innerHTML);
    if (points_left < 0){
	alert('Вы потратили больше 25 очков.');
	return 0;
    }

    // globals
    var main = document.getElementById("main");
    //main.innerHTML = '';
    window.preds_dead = 0;
    window.herbs_dead = 0;
    window.max_w = 1295;
    window.max_h = 795;
    window.m1 = new Array();
    window.FIELD = document.getElementById('main');
    window.food = [];


    var e = document.getElementById("grass_lands");
    grasslands_count = Number(e.options[e.selectedIndex].value); 

    for (i=0; i < grasslands_count; i++) {
	k = init_field("f-"+i);
	food[i] = k;
    }

    var e = document.getElementById("level");
    level = Number(e.options[e.selectedIndex].value); 
    window._level = level;
    
    predators_count = level * 3 + 10; //Number(document.getElementById("predators").value);

    var e = document.getElementById("amount");
    amount = Number(e.options[e.selectedIndex].value); 
    
    herbivores_count = amount; //Number(document.getElementById("herbivores").value);


    pred_mut_rate = level;
    
    for (counter = 0; counter < predators_count; counter++) {
	var t = init_object(1, counter, FIELD, pred_mut_rate);
	t.speed = level * 2 + 3;
	t.attack = level * 2 + 6 - level;
	t.see_range = level * 2 + 20;
	m1[counter] = t;
	last_c = counter;
    }

    var e = document.getElementById("mutation_rate");
    window._mut_rate = Number(e.options[e.selectedIndex].value); 

    var e = document.getElementById("fertility_age");
    window._fertility_age = Number(e.options[e.selectedIndex].value); 

    var e = document.getElementById("see_range");
    window._see_range = Number(e.options[e.selectedIndex].value); 

    var e = document.getElementById("speed");
    window._speed = Number(e.options[e.selectedIndex].value); 

    var e = document.getElementById("defense");
    window._defense = Number(e.options[e.selectedIndex].value); 

    
    for (counter = 0; counter < herbivores_count; counter++) {
	pos = last_c + counter;
	var t = init_object(2, pos, FIELD, _mut_rate);
	t.fertility_age = _fertility_age;
	t.speed = _speed;
	t.defense = _defense;
	t.see_range = _see_range;
	m1[pos] = t;
    }
    window.d_hnd = setTimeout(draw_all, 500);

}

function draw_all(){
    
    
    var c = document.getElementById("mC");
    var ctx = c.getContext("2d");
    ctx.save();
    ctx.fillStyle = '#FDFFDA';//#FFFFFF';
    ctx.fillRect(0, 0, max_w+5, max_h+5);
    ctx.restore();
    var grass_img = document.getElementById("grass");
    var hunter_img = document.getElementById("hunter");
    var plant_eater_img = document.getElementById("plant-eater");

    for (var i = 0; i < food.length; i++) {
	if (food[i].total > 0){
	    ctx.drawImage(grass_img, food[i].x - 35, food[i].y - 23, 70, 45);
	}
    }
    ctx.font = "10px Arial";
    ctx.fillText('Day: ' + DAY +' SP: ' + SPEED, 10, 10);     
    
    for (var i = 0; i < m1.length; i++) {
	if(!m1[i]){continue;}
	if(m1[i].is_dead){continue;}
	ctx.beginPath();
	ctx.arc(m1[i].x,m1[i].y,2, 0, 2*Math.PI);
	if (m1[i].otype == 1){
	    ctx.drawImage(hunter_img, m1[i].x - 7, m1[i].y - 6, 15, 10);
	} else {
	    ctx.drawImage(plant_eater_img, m1[i].x - 7, m1[i].y - 7, 15, 14);
	}
	//ctx.stroke();
    }
    window.d_hnd = setTimeout(draw_all, 250);

    // clear array
    for (var i = 0; i < m1.length; i++) {
        if (!m1[i]){
            m1.splice(i, 1);
        }
    }
}
function move_all(){
    DAY += 1;
    //console.log(m1);
    let t1 = performance.now();
    
    for (var i = 0; i < food.length; i++) {
	if (food[i].total < 1){
	    food[i].grow_time -= 1;
	    if (food[i].grow_time < 1){
		food[i].birth();
	    }
	}
	
    }
    // main loop
    for (var i = 0; i < m1.length; i++) {
	if (!m1[i]){
            continue;
        }
	if (m1[i].is_dead == true) {
	    continue;
	}
	//console.log(m1[i].otype);
	//console.log(i);
	m1[i].move();
	if (!m1[i]){continue;}
	m1[i].feed();
        
	for (var j = 0; j < m1.length; j++) {
	    if (i != j ){
		if (!m1[j]){
		    continue;
		}
		if (m1[j].is_dead == true) {
		    continue;
		}
                if (Math.abs(m1[i].x - m1[j].x) > 15 ){
                    continue; // too far away
                }
                if (Math.abs(m1[i].y - m1[j].y) > 15 ){
                    continue; // too far away
                }
		collision(m1[i], m1[j]);
	    }
	}
    }
    window.m_hnd = setTimeout(move_all, 50);
    SPEED = performance.now() - t1;
}

function log_stat(){
    max_age = 0;
    max_age_pred = 0;
    preds = 0;
    herbs = 0;
    dead = 0;
    dead_pred = 0;
    gen = 0;
    gen_pred = 0;
    max_def = 0;
    max_att = 0;
    max_see = 0;
    max_speed = 0;
    max_see_pred = 0;
    max_speed_pred = 0;
    
    for (var i = 0; i < m1.length; i++) {
	if (!m1[i]){
	    continue;
	}
	if (m1[i].is_dead){
	    continue;
	}

	if (m1[i].otype == 1){
	    preds += 1;
	    if (m1[i].attack > max_att){
		max_att = m1[i].attack;
	    }
	    if (m1[i].see_range > max_see_pred){
		max_see_pred = m1[i].see_range;
	    }
	    if (m1[i].speed > max_speed_pred){
		max_speed_pred = m1[i].speed;
	    }

	    if (m1[i].age > max_age_pred){
		max_age_pred = m1[i].age;
	    }
	    if (m1[i].gen > gen_pred){
		gen_pred = m1[i].gen;
	    }
	    
	}
	if (m1[i].otype == 2){
	    herbs += 1;
	    if (m1[i].defense > max_def){
		max_def = m1[i].defense;
	    }
	    if (m1[i].see_range > max_see){
		max_see = m1[i].see_range;
	    }
	    if (m1[i].speed > max_speed){
		max_speed = m1[i].speed;
	    }
	    
	    if (m1[i].age > max_age){
		max_age = m1[i].age;
	    }
	    if (m1[i].gen > gen){
		gen = m1[i].gen;
	    }
	}
    }

    dlog_clear();
    
    dlog('Макс возраст: ' + max_age_pred);
    dlog('Макс дальность видения: ' + max_see_pred);
    dlog('Макс скорость: ' + max_speed_pred);
    dlog('Макс поколение: ' + gen_pred);
    dlog('Макс сила атаки: ' + max_att);
    dlog('Погибло: ' + preds_dead);
    dlog('Леопарды: ' + preds);
    dlog('--------------');
    
    dlog('Макс. возраст: ' + max_age);
    dlog('Макс. дальность видения: ' + max_see);
    dlog('Макс скорость: ' + max_speed);
    dlog('Макс. поколение: ' + gen);
    dlog('Макс. защита ' + max_def);
    dlog('Погибло: ' + herbs_dead);
    dlog('Слоны: ' + herbs);
    dlog('--------------');

    window.log_hnd = setTimeout(log_stat, 10000);

    if (preds == 0){
	end_game();
	alert('Слоны победили!');
    }
    if (herbs == 0){
	end_game();
	alert('Леопарды победили!');
    }
}

