function rnd(start, end){
    return Math.floor(Math.random()*(end-start+1))+start;
}

function dist(x1, y1, x2, y2){
    var a = x1 - x2
    var b = y1 - y2
    return Math.sqrt( a*a + b*b );    
}

function dlog(text){
    el = document.getElementById("log");
    inh = el.innerHTML;
    el.innerHTML = text + "<br />" + inh;
}
function dlog_clear(text){
    el = document.getElementById("log");
    el.innerHTML = "";
}

// plants field
function init_field(id){
    var t = Object.create(null);
    t.total = 50000 + rnd(1, 40000);
    t.id = id
    t._id = "#" + id;
    t.x = rnd(1, max_w - 50);
    t.y = rnd(1, max_h - 50);
    t.base_class = "f";
    t.grow_time_init = rnd(1, 150);
    t.grow_time = rnd(1, 150);
    
    t.eat = function (amount){
	this.total -= amount;
	if (this.total < 1){
	    this.die(); // actually move...
	    return false;
	}
	return true;
    };
    t.die = function (){
	//el = document.getElementById(this.id);
	//el.setAttribute('class', this.base_class + " f5")
	
    };
    t.birth = function (){
	this.x = rnd(1, max_w - 70);
	this.y = rnd(1, max_h - 70);
        this.total = 50000 + rnd(1, 40000);
	this.grow_time = this.grow_time_init;
	//el = document.getElementById(this.id);
	//move(el).x(x).y(y).end();
	//el.setAttribute('class', this.base_class + " f1")
    }
    return t
}



function init_object(otype, pos, field, mutation_rate){
    //var d = document.createElement("div");
    var t = Object.create(null);
    t.id = "c-" + counter;
    t.age = 0;
    t.speed = 2;
    t.gen = 1;
    t.max_age_mutation = 1000;
    t.fertility_calmdown = 150;
    t.max_fertility_calmdown = 150;
    if (otype == 1){
	
    } else {
	
    }
    t.max_age = 2000 + rnd(1, t.max_age_mutation);

    mutation_rate = (typeof mutation_rate !== 'undefined') ?  mutation_rate : 1;
    t.mutation = mutation_rate;
    t.fertility_age = 300;
    t.defense = 1;
    t.attack = 3;
    t.see_range = 70;
    t.otype = otype;
    if (otype == 2){
	// globals
	t.mutation = _mut_rate;
    }
    if (otype == 1){
	// globals
	t.fertility_age = 330 - _level * 40;
    }
    
    t._id = "#" + t.id;
    t.pos = pos;
    t.x = rnd(0, max_w-5);
    t.y = rnd(0, max_h-5);
    t.is_dead = false;
    t.fulness = 500;
    t.hungry = 1000;
    t.max_fulness = 2500;
    
    t.direction = rnd(1, 4);
    //d.setAttribute('id', t.id);
    //field.appendChild(d);


    // check if we hungry
    t.is_hungry = function(){
        if (this.fulness > this.hungry * 1.2) {
            return false;
        }
        return true;
    }
    // mut func
    t.mut = function(){
	ch = 0;
	while (ch == 0){
	    ch = rnd(-1 * this.mutation, 1 * this.mutation);
	}
	return ch;
    }
    // feed functions
    t.feed = function(tt) {
	if (this.otype == 1){
	    if (!tt) {return 0;}
	    if (this.is_hungry() == false) {
		// ch_ = tt.attack - tt.defense;
		// if (ch_ < 1){
		//     ch_ = 0;
		// }
            // do not want to eat
            } else {
		if (this.attack >= tt.defense && rnd(60, 100 * ch_) > 90){
		    this.fulness = this.fulness + tt.fulness + 1300;
		    if (this.fulness > this.max_fulness){
			this.fulness = this.max_fulness;
		    } 
		    tt.die();
		    //console.log('Predator ate some grasseater' + rnd(1, 100))
		    //dlog("");
		} else {
		    //dlog("Predator was too weak, and now dead");
		    if (rnd(1, 100) > 97){
			//console.log('Predator dead by grasseater'+ rnd(1, 100))
			this.die();
		    }
		    // grasseater now weaker
		    tt.defense = tt.defense -= 1;
		    if (tt.defense < 1){
			tt.defense = 1;
		    }
		}
	    }
	}
	if (this.otype == 2){
	    for (var i = 0; i < food.length; i++) {
		if (this.x > food[i].x-25 && this.x < food[i].x +25){
		    if (this.y > food[i].y-25 && this.y < food[i].y +25){
			if (food[i].eat(50)){
			    this.fulness = this.fulness + 50;
			}
			
			if (this.fulness > this.max_fulness){
			    this.fulness = this.max_fulness;
			}
		    }
		}
	    }
	}
	
    }
    t.grow = function (){
	// grow size, age
	// s1 -> 
	this.age = this.age + 1;
	if (this.fertility_calmdown > 0){
	    this.fertility_calmdown -= 1;
	}

	if (Math.floor(this.max_age/this.age) < 10) {
	    this.defense 
	}
	
    }
    // Die function
    t.die = function (){
	if (this.otype == 1){
	    preds_dead += 1;
	} else {
	    herbs_dead += 1;
	}

	this.is_dead = true;
	m1[this.pos] = false; //Object.create(null);// clear object
        
    };
    // Move function
    t.move = function (){
	if (this.is_dead == true){
	    return 0;
	}
	x = this.x;
	y = this.y;
	this.grow();
	if (this.speed > 0){
	    ch_ = Math.floor(this.speed/2) * 3;
	} else { ch_ = 0; }

	if (this.otype == 1){
	    ch2 = ch_ + 4;
	    if (ch2 < 2 ){ ch2 = 1;}
	    if (ch2 > 10){ ch2 = 10;}
            
	} else {
	    ch2 = ch_ + 1
	    if (ch2 < 2 ){ ch2 = 1;}
	    //if ch
	    this.fulness = this.fulness - ch2;
	}
	
	if (this.age > this.max_age){
	    //dlog("Dead by age");
	    //console.log('Dead by age: '+this.otype);
	    this.die();
	    return 0;
	}
	if (this.fulness < 1){
	    this.die();
	    console.log('Hungry dead: '+this.otype + ' |' + rnd(1, 100));
	    
	    //dlog("Hungry dead");
	    return 0;
	}
	//console.log(this.age);
	//console.log(x, y);
	if (rnd(1, 2) == 1){
	    x = x + this.speed;
	} else {
	    x = x - this.speed;
	}

	if (rnd(1, 2) == 1){
	    y = y + this.speed;
	} else {
	    y = y - this.speed;
	}
	//}
	
	direction_found = false;
	if (this.otype == 2) { // looking for plants
	    for (var i = 0; i < food.length; i++) {
		if (dist(x, y, food[i].x, food[i].y) < this.see_range) {
		    if (food[i].total < 50){
			continue;
		    }
		    direction_found = true;
		    if (x < food[i].x){
			x += this.speed;
		    } else {
			x -= this.speed;
		    }
		    if (y < food[i].y){
			y += this.speed;
		    } else {
			y -= this.speed;
		    }
		    
		}
	    }
	} else { // looking for elephants
	    for (var i = 0; i < m1.length; i++) {
		if (direction_found == true){
		    continue;
		}
		if (this.fulness > this.max_fulness - 200){
		    continue; // random move if we are full
		}
		if (!m1[i]){continue;}
		if (m1[i].is_dead){continue;}
		
		if (dist(x, y, m1[i].x, m1[i].y) < this.see_range) {
		    if (this.fertility_calmdown < 5) {
			if (this.fulness > this.max_fulness / 2 && m1[i].otype == 2){
			    // lets fuck
			    continue;
			}
			if (m1[i].otype == 1 && this.fulness > this.max_fulness /2) {
			    // lets fuck
			} else {
			    // too hungry for copulation
			    continue;
			}
		    }
		    
		    direction_found = true;
		    if ((Math.abs(m1[i].x - this.x) < this.speed) && (Math.abs(m1[i].y - this.y) < this.speed)) {
			this.x = m1[i].x;
			this.y = m1[i].y;
			
		    } else {
			if (x < m1[i].x){
			    x += this.speed;
			} else {
			    x -= this.speed;
			}
			if (y < m1[i].y){
			    y += this.speed;
			} else {
			    y -= this.speed;
			}
		    }
		}
	    }

	}
	//if (!direction_found){

	if (x >= max_w) {
	    x = x - this.speed;
	    this.direction = rnd(2, 3);
	}
	if (x < 1) {
	    x = x + this.speed;
	    while (this.direction == 2){
		this.direction = rnd(1, 3);
	    }
	    
	}
	if (y >= max_h) {
	    while (y >= max_h){
		y = y - this.speed;
	    }
	    while (this.direction == 3){
		this.direction = rnd(1, 4);
	    }
	}
	if (y < 1) {
	    y = y + this.speed;
	    while (this.direction == 4){
		this.direction = rnd(1, 3);
	    }
	    
	}
	if (!direction_found){
	    if (this.direction == 1){
		x += this.speed;
	    }
	    if (t.direction == 2){
		x -= this.speed;
	    }
	    if (t.direction == 3){
		y += this.speed;
	    }
	    if (t.direction == 4){
		y -= this.speed;
	    }
	    
	}
	this.x = x;
	this.y = y;
	//console.log(this);
	//elem = document.getElementById(this.id);
	//move(elem).x(x).y(y).end();
    }
    return t;
}

// Birth
function birth(t1, t2){
    if (t1.fertility_age > t1.age || t2.fertility_age > t2.age){
	// too young
	return 0;
    }
    if (t1.age/t1.max_age >0.8 || t2.age/t2.max_age>0.8){
	// too old
	return 0;
    }
    
    if (t1.fertility_calmdown != 0 || t2.fertility_calmdown != 0){
	return 0;
    }
    if (t1.fulness < t1.hungry){
	return 0;
    }
    if (t2.fulness < t2.hungry){
	return 0;
    }
    let p = 5;
    if (t1.otype == 1){
	p = 10;
    }
    if (rnd(1, p) == 1){
	//dlog("New baby!");
	t1.fertility_calmdown = t1.max_fertility_calmdown;
	t2.fertility_calmdown = t2.max_fertility_calmdown;
	counter = m1.length + 1;
	//console.log(counter);
	var n_t = init_object(t1.otype, counter, FIELD);
        if (Math.round(Math.random())){
	    n_t.speed = t1.speed + n_t.mut();
        } else {
	    n_t.speed = t1.speed - n_t.mut();
        }
        if (n_t.speed > 4){
            n_t.speed = 4; // Max speed is now 4
        }
        if (Math.round(Math.random())){
	    n_t.fertility_age = Math.floor((t1.fertility_age+t2.fertility_age)/2) + n_t.mut();
        } else {
	    n_t.fertility_age = Math.floor((t1.fertility_age+t2.fertility_age)/2) - n_t.mut();
        }
            
	
	if (n_t.speed < 0){
	    n_t.speed = 0;
	}
        
        if (Math.round(Math.random())){
	    n_t.defense = Math.floor((t1.defense+t2.defense)/2) + Number(n_t.mut());
        } else {
	    n_t.defense = Math.floor((t1.defense+t2.defense)/2) - Number(n_t.mut());
        }            

	n_t.max_age = Math.floor((t1.max_age + t2.max_age)/2) + Number(n_t.mut()) * 10;
	
	if (n_t.defense < 0){
	    n_t.defense = 0;
	}
	n_t.attack = t1.attack + Number(n_t.mut());
	if (n_t.attack < 0){
	    n_t.attack = 0;
	}
	n_t.see_range = t1.see_range + Number(n_t.mut()) * 2;
	if (n_t.see_range < 0){
	    n_t.see_range = 0;
	}
	
	n_t.max_fertility_calmdown += rnd(-2*t1.mutation, 2*t1.mutation);
	
	
	n_t.x = t1.x;
	n_t.y = t1.y;
	t1.fulness = t1.fulness - 50;
	t2.fulness = t2.fulness - 50;
	
	if (t1.gen > t2.gen){
	    n_t.gen = t1.gen + 1;
	} else {
	    n_t.gen = t2.gen + 1;
	}
	m1[counter] = n_t;
    }

}
// collision function

function collision(t1, t2){
    coll = false;
    if (t1.otype == 1){
	if (t1.x > t2.x-t1.speed && t1.x < t2.x + t1.speed) {
	    if (t1.y > t2.y-t1.speed && t1.y < t2.y + t1.speed) {
		coll = true;
	    }
	}
    }
    if (t2.otype == 1){
	if (t1.x > t2.x-t2.speed && t1.x < t2.x + t2.speed) {
	    if (t1.y > t2.y-t2.speed && t1.y < t2.y + t2.speed) {
		coll = true;
	    }
	}
    }
    if (t2.otype == 2 && t1.otype == 2){
	if (t1.x > t2.x-t2.speed && t1.x < t2.x + t2.speed) {
	    if (t1.y > t2.y-t2.speed && t1.y < t2.y + t2.speed) {
		coll = true;
	    }
	}
    }

    if (coll){
	if (t1.otype == t2.otype) {
            if (t1.otype == 2) { //
                max_babies = rnd(1, 5);
            } else {
                max_babies = rnd(1, 2)
            }
            for (let i = 0; i < max_babies; i++) {
	        birth(t1, t2);
            }
	    
	    //var last = m1.length + 1;
	    //console.log("same", t1.otype, t1.id, t2.otype, t2.id);
	    
	} else {
	    //console.log(t1,t2);
	    if (t1.otype == 1){
		t1.feed(t2);
	    } else {
		t2.feed(t1);
	    }
	}
    }
}
